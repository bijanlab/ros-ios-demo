//
//  ContentView.swift
//  rosdemo
//
//  Created by Jarl Haggerty on 12/27/20.
//

import SwiftUI
import BSON

struct ContentView: View {
    @State private var duration = 0
    @State private var running = false
    private var ros = Ros("ws://192.168.1.3:9090")
    
    var body: some View {
        VStack {
            Button(action: {
                self.onClicked()
            }) {
                if running {
                    Text("Click me, \(duration) seconds left")
                        .padding()
                } else {
                    Text("Waiting for task")
                        .padding()
                }
            }
            
        }.onAppear {
            self.startup()
        }.onDisappear() {
            print("Disappeared")
        }
    }
    
    func startup() {
        ros.start()
        ros.advertiseTopic("/task_executor", "task_controller_interfaces/TaskResult")
        ros.subscribeTopic("/task_controller", "std_msgs/String") {
            msg in
            let data = msg["data"] as! String
            do {
                let jsonAny = try JSONSerialization.jsonObject(with: data.data(using: .utf8)!)
                let json = jsonAny as! [String : Any]
                try self.onTaskReceived(config: json)
            } catch {
                print("Task processing error")
            }
        }
        ros.subscribeTopic("/add_time", "std_msgs/String") {
            msg in
            duration += 10
        }
        
        let _ = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            self.ros.publish(["success": false, "done": !running], "/task_executor")
            if !running {
                return
            }
            duration -= 1
            
            if duration == 0 {
                running = false
            }
            
            if !running {
                let request: Document = [
                    "path": "`nth_task(0)`.duration"
                ]
                self.ros.callService("/task_controller/get_field", request) { (response) in
                    do {
                        let data = response["value"] as! String
                        let jsonAny = try JSONSerialization.jsonObject(with: data.data(using: .utf8)!)
                        var json = jsonAny as! [String: Any]
                        let min = json["min"] as! Double
                        let max = json["max"] as! Double
                        json["min"] = min + 1
                        json["max"] = max + 1
                        let jsonText = try JSONSerialization.data(withJSONObject: json)
                        
                        self.ros.callService("/task_controller/set_field", [
                            "path": "`nth_task(0)`.duration",
                            "value": String(decoding: jsonText, as: UTF8.self)]) {
                            response in
                            self.ros.publish(["success": false, "done": !running], "/task_executor")
                        }
                    } catch {
                        print("Call response error")
                    }
                }
            }
        }
    }
    
    func onTaskReceived(config: [String: Any]) throws {
        let temp = try getDouble(config: config, key: "duration")
        let temp2 = Int(temp)
        duration = temp2
        running = true
    }
    
    func onClicked() {
        duration = 0
        running = false
        
        do {
            let json = ["min": 2, "max": 3]
            let jsonText = try JSONSerialization.data(withJSONObject: json)
            self.ros.callService("/task_controller/set_field", [
                "path": "`nth_task(0)`.duration",
                "value": String(decoding: jsonText, as: UTF8.self)]) {
                response in
                self.ros.publish(["success": true, "done": true], "/task_executor")
            }
        } catch {
            print("JSON serialization failed")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
