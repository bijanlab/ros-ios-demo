//
//  rosdemoApp.swift
//  rosdemo
//
//  Created by Jarl Haggerty on 12/27/20.
//

import SwiftUI

@main
struct rosdemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
