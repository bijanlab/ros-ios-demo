//
//  ros.swift
//  rosdemo
//
//  Created by Jarl Haggerty on 12/27/20.
//

import Foundation
import BSON

enum ProgramError: Error {
    case runtimeError(message: String)
}

func getDouble(config: [String: Any], key: String) throws -> Double {
    if let number = config[key] as? Double {
        return number
    }
    
    let field = config[key] as! [String: Any]
    
    if let min = field["min"] as? Double {
        if let max = field["max"] as? Double {
            let result = Double.random(in: min..<max)
            return result
        }
    }
    
    throw ProgramError.runtimeError(message: "Unable to read double from \(field)")
}

class Ros {
    private var webSocket: URLSessionWebSocketTask
    private var topicCallbacks: [String: (Document) -> Void] = [:]
    private var serviceCallbacks: [String: (Document) -> Void] = [:]
    private var nextId = 0
    
    init(_ rosbridgeUrl: String) {
        webSocket = URLSession.shared.webSocketTask(with: URL(string: rosbridgeUrl)!)
    }
    
    func start() {
        webSocket.resume()
        receive()
    }
    
    func receive() {
        print("Receiving")
        webSocket.receive { result in
            print("Received")
            switch result {
            case .success(let message):
              switch message {
              case .data(let data):
                    let document = Document(data: data)
                    let op = document["op"] as! String
                    if op == "status" {
                        let level = document["level"] as! String
                        let msg = document["msg"] as! String
                        print("\(level) \(msg)")
                    } else if op == "publish" {
                        let topic = document["topic"] as! String
                        let msg = document["msg"] as! Document
                        if let callback = self.topicCallbacks[topic] {
                            callback(msg)
                        }
                    } else if op == "service_response" {
                        let id = document["id"] as! String
                        if let callback = self.serviceCallbacks[id] {
                            if let values = document["values"] as? Document {
                                callback(values)
                            } else {
                                callback(Document())
                            }
                        }
                    }
              case .string(let text):
                print("Text received \(text)")
              }
            case .failure(let error):
              print("Error when receiving \(error)")
            }
            
            self.receive()
        }
    }
    
    func advertiseTopic(_ topic: String, _ type: String) {
        let doc: Document = [
            "op": "advertise",
            "topic": topic,
            "type": type
        ]
        let advertiseMessage = URLSessionWebSocketTask.Message.data(doc.makeData())
        webSocket.send(advertiseMessage) {
            error in
            if let error = error {
                print("Error when advertising \(error)")
            }
        }
    }

    func subscribeTopic(_ topic: String, _ type: String, _ callback: @escaping (Document) -> Void) {
        let doc: Document = [
            "op": "subscribe",
            "topic": topic,
            "type": type
        ]
        let subscribeMessage = URLSessionWebSocketTask.Message.data(doc.makeData())
        webSocket.send(subscribeMessage) {
            error in
            if let error = error {
                print("Error when advertising \(error)")
            }
        }
        
        topicCallbacks[topic] = callback
    }
    
    func callService(_ service: String, _ request: Document, _ callback: @escaping (Document) -> Void) {
        let callId = String(nextId)
        nextId += 1
        let doc: Document = [
            "op": "call_service",
            "service": service,
            "args": request,
            "id": callId
        ]
        
        let message = URLSessionWebSocketTask.Message.data(doc.makeData())
        webSocket.send(message) {
            error in
            if let error = error {
                print("Error when calling service \(error)")
            }
        }
        
        serviceCallbacks[callId] = callback
    }
    
    func publish(_ msg: [String: Primitive], _ topic: String) {
        var msgDocument = Document()
        for (key, value) in msg {
            msgDocument[key] = value
        }
        let doc: Document = [
            "op": "publish",
            "topic": topic,
            "msg": msgDocument
        ]
        
        let message = URLSessionWebSocketTask.Message.data(doc.makeData())
        webSocket.send(message) {
            error in
            if let error = error {
                print("Error when publishing message \(error)")
            }
        }
    }
}
